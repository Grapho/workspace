"""
Модуль запуска приложения.
Отвечает за вызов всех необходимых компонентов.
"""

from src.general import timing
from src.general import Printer
from src.general import INFO
from src.general import ERRORS

from src.manage import Manage


@timing
def main():
    # Порядок действий:
    # вызываем менеджера, который запустит весь процесс.
    # В этом модуле будет только приветствие, запуск приложения и прощание.
    # Весь конструктивный функционал будет разделен по разным модулям.
    manage = Manage().manage()
    if not manage.success:
        Printer.error_print(manage)
        return
    Printer.casual_print(INFO.OKAY)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        Printer.casual_print(ERRORS.CTRL_C)
