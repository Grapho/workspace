"""
Модуль для общих функций/классов всего приложения.
Содержит элементы, с которыми можно взаимодействовать из любого другого модуля.
"""

from pprint import pprint
from datetime import datetime


class Constant:
    """
    Класс для константных значений
    """
    SYSTEM_ERR_PREFIX = '[SYSTEM ERROR]:'
    SYSTEM_WRNG_PREFIX = '[SYSTEM WARNING]:'
    SYSTEM_INFO_PREFIX = '[SYSTEM INFO]:'
    SEPARATOR = '====================='
    DAY_PRINT = ' [{}] '


class ERRORS:
    """
    Класс, содержащий константные ошибки
    """
    CTRL_C = ' '.join([Constant.SYSTEM_ERR_PREFIX, 'Stopped. Got Ctrl+C'])


class WARNINGS:
    """
    Класс, содержащий константные предупреждения
    """
    PERSON_DEAD = ' '.join([Constant.SYSTEM_WRNG_PREFIX, '{} - are dead'])


class INFO:
    """
    Класс, содержащий константные информационные сообщения
    """
    OKAY = ' '.join([Constant.SYSTEM_INFO_PREFIX, 'Everything went okay'])
    ELAPSED_TIME = ' '.join([Constant.SYSTEM_INFO_PREFIX, 'elapsed time: {}'])
    BIRTH = ' '.join([Constant.SYSTEM_INFO_PREFIX, '{} was born right now'])


class Response:
    """
    Lower level API.
    :result:
        type: any object
        desc: Function execution result. None if the result is failure
    :success:
        type: boolean
        desc: Boolean variable. True - if success, False - if failure
            you can use it something like that:
                do_smth() if Response().success else do_not_smth()
    :message:
        type: string
        desc: Accompanying message about execution results.
    """
    result = None
    success = False
    message = str()

    def __str__(self):
        """
        Переопределение базовой функции презентации класса.
        Возвращает self.message
        """
        return str(self.message) if self.message else str(result)


class Printer:
    """
    Класс, который отвечает за вывод сообщений в консоль/лог файл
    """
    @classmethod
    def casual_print(self, msg):
        """
        Обычный вывод в консоль и лог-файл
        """
        pprint(msg)

    @classmethod
    def error_print(self, target):
        """
        Вывод сообщения об ошибке в консоль и лог-файл
        """
        pprint(' '.join([
            Constant.SYSTEM_ERR_PREFIX, str(target)]))


def check2desu(foo):
    """
    Декоратор для проверки успешности выполнения функции.
    Используется API нижнего уровня.
    :return: экземпляр класса Response
    """

    def wrapper(self, *args, **kwargs):
        resp = Response()
        try:
            tmp = foo(self, *args, **kwargs)
            resp.success = True
            resp.result = tmp
        except Exception as err:
            resp.success = False
            resp.message = err
        return resp
    return wrapper


def timing(foo):
    """
    Декоратор для засечения времени выполнения функции.
    Выводит в консоль сообщение с информацией о потраченном
        на выполнение функции времени.
    """
    def wrapper():
        start = datetime.now()
        Printer.casual_print(Constant.SEPARATOR)
        foo()
        Printer.casual_print(Constant.SEPARATOR)
        Printer.casual_print(INFO.ELAPSED_TIME.format(datetime.now() - start))
    return wrapper
