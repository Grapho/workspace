"""
Модуль, содержащий описание класса человека.
"""
from src.general import Printer
from src.general import INFO
from src.general import WARNINGS


class Person:
    """
    Класс человека
    """
    id = 0

    def __init__(self, name, parents, life):
        self.life = life
        self.birth_date = None
        self.height = None
        self.weight = None
        self.race = None
        self.gender = None
        self.country = None
        self.mood = None
        self.name = name
        self.surname = parents[0].surname
        self.mother = None
        self.father = None
        self.health = None

        self.alive = True
        self.__awake__()

    def get_age(self):
        """
        Функция возвращает текущий возраст этого человека.
        """
        pass

    def __str__(self):
        # имя, фамилия, возраст, пол
        return f'Name: {self.name}, Surname: {self.surname},' \
            ' Age: {self.get_age}, Gender: {self.gender}'

    def __awake__(self):
        Printer.casual_print(
            INFO.BIRTH.format(self.name)
        )
        self.life()

    def live(self):
        if not self.health:
            self.alive = False
            raise WARNINGS.PERSON_DEAD.format(str(self))
